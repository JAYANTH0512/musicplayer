package com.sunbeam;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class GaanamvcApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(GaanamvcApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("DbInit will be done by url /admin/init/sampledb!");
	}
}


