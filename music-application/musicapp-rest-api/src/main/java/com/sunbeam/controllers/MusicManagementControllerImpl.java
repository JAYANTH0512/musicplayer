package com.sunbeam.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dtos.AlbumFormDTO;
import com.sunbeam.dtos.ArtistFormDTO;
import com.sunbeam.dtos.DtoEntityConverterImpl;
import com.sunbeam.dtos.GaanaResponse;
import com.sunbeam.dtos.SongFormDTO;
import com.sunbeam.entities.Album;
import com.sunbeam.entities.Artist;
import com.sunbeam.entities.Song;
import com.sunbeam.services.MusicServiceImpl;
import com.sunbeam.services.StorageService;

@RequestMapping("/admin")
@CrossOrigin
@RestController
public class MusicManagementControllerImpl {
	@Autowired
	private MusicServiceImpl musicService;
	@Autowired
	private DtoEntityConverterImpl dtoEntityConverter;
	@Autowired
	private StorageService storageService;
		
	@PostMapping("/artists")
	public ResponseEntity<GaanaResponse> saveArtist(ArtistFormDTO artistDto) throws IOException {
		Artist artist = dtoEntityConverter.artistFormDtoToEntity(artistDto);
		String thumbnail = storageService.store(artistDto.getThumbnail());
		artist.setThumbnail(thumbnail);
		musicService.saveArtist(artist);
		return GaanaResponse.success(artist);
	}
	
	@PostMapping("/albums")
	public ResponseEntity<GaanaResponse> saveAlbum(AlbumFormDTO albumDto) throws IOException {
		Album album = dtoEntityConverter.albumFormDtoToEntity(albumDto);
		String thumbnail = storageService.store(albumDto.getThumbnail());
		album.setThumbnail(thumbnail);
		musicService.saveAlbum(album);
		return GaanaResponse.success(album);
	}

	@RequestMapping("/songs")
	public ResponseEntity<GaanaResponse> saveSong(SongFormDTO songDto) {
		Song song = dtoEntityConverter.songFormDtoToEntity(songDto);
		String file = storageService.store(songDto.getFile());
		song.setFile(file);
		musicService.saveSong(song);
		return GaanaResponse.success(song);
	}
}


