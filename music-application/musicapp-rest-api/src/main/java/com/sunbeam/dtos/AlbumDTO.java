package com.sunbeam.dtos;

public class AlbumDTO {
	private int id;
	private String title;
	private String thumbnail;

	public AlbumDTO() {
	}

	public AlbumDTO(int id, String title, String thumbnail) {
		this.id = id;
		this.title = title;
		this.thumbnail = thumbnail;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	@Override
	public String toString() {
		return "AlbumDTO [id=" + id + ", title=" + title + ", thumbnail=" + thumbnail + "]";
	}
}
