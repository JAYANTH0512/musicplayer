package com.sunbeam.dtos;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.sunbeam.entities.Album;
import com.sunbeam.entities.Artist;
import com.sunbeam.entities.Song;

@Component
public class DtoEntityConverterImpl {
	public Artist artistFormDtoToEntity(ArtistFormDTO artistDto) {
		Artist artist = new Artist();
		BeanUtils.copyProperties(artistDto, artist, "thumbnail");
		if(artistDto.getThumbnail() != null)
			artist.setThumbnail(artistDto.getThumbnail().getOriginalFilename());
		return artist;
	}

	public Album albumFormDtoToEntity(AlbumFormDTO albumDto) {
		Album album = new Album();
		BeanUtils.copyProperties(albumDto, album, "thumbnail");
		if(albumDto.getThumbnail() != null)
			album.setThumbnail(albumDto.getThumbnail().getOriginalFilename());
		return album;
	}

	public Song songFormDtoToEntity(SongFormDTO songDto) {
		Song song = new Song();
		BeanUtils.copyProperties(songDto, song, "file");
		if(songDto.getFile() != null)
			song.setFile(songDto.getFile().getOriginalFilename());
		if(songDto.getAlbum() != null)
			song.setAlbum(albumFormDtoToEntity(songDto.getAlbum()));
		if(songDto.getArtistList() != null) {
			List<Artist> artistList = songDto.getArtistList()
					.stream()
					.map(arDto -> artistFormDtoToEntity(arDto))
					.collect(Collectors.toList());
			song.setArtistList(artistList);
		}
		return song;
	}

	public ArtistDTO artistEntityToDto(Artist artist) {
		if(artist == null)
			return null;
		ArtistDTO dto = new ArtistDTO();
		BeanUtils.copyProperties(artist, dto);
		return dto;
	}

	public AlbumDTO albumEntityToDto(Album album) {
		if(album == null)
			return null;
		AlbumDTO dto = new AlbumDTO();
		BeanUtils.copyProperties(album, dto);
		return dto;
	}

	public SongDTO songEntityToDto(Song song, boolean deepCopy) {
		if(song == null)
			return null;
		SongDTO dto = new SongDTO();
		BeanUtils.copyProperties(song, dto);
		if(deepCopy)
			dto.setAlbum(albumEntityToDto(song.getAlbum()));
		if(deepCopy) {
			List<ArtistDTO> artistList = song.getArtistList()
					.stream()
					.map(ar -> artistEntityToDto(ar))
					.collect(Collectors.toList());
			dto.setArtistList(artistList);
		}
		return dto;
	}
}
