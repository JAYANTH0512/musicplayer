package com.sunbeam.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sunbeam.entities.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {
	User findByEmail(String email);
}
