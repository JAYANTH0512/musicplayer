package com.sunbeam.dtos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class SongFormDTO {
	private int id;
	private String title;
	private MultipartFile file;
	private String duration;
	private AlbumFormDTO album;
	private List<ArtistFormDTO> artistList;

	public SongFormDTO() {
		this.artistList = new ArrayList<>();
	}
	public SongFormDTO(int id, String title, MultipartFile songFile, String duration) {
		this.id = id;
		this.title = title;
		this.file = songFile;
		this.duration = duration;
		this.artistList = new ArrayList<>();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public AlbumFormDTO getAlbum() {
		return album;
	}
	public void setAlbum(AlbumFormDTO album) {
		this.album = album;
	}
	
	public List<ArtistFormDTO> getArtistList() {
		return artistList;
	}
	public void setArtistList(List<ArtistFormDTO> artistList) {
		this.artistList = artistList;
	}
	
	@Override
	public String toString() {
		return "SongFormDTO [id=" + id + ", title=" + title + ", file=" + file + ", duration=" + duration + "], " +
				"album " + (album == null ? null : album.getId());
	}
}
