package com.sunbeam.dtos;

import java.util.ArrayList;
import java.util.List;

public class SongDTO {
	private int id;
	private String title;
	private String file;
	private String duration;
	private AlbumDTO album;
	private List<ArtistDTO> artistList;

	public SongDTO() {
		this.artistList = new ArrayList<>();
	}
	public SongDTO(int id, String title, String songFile, String duration) {
		this.id = id;
		this.title = title;
		this.file = songFile;
		this.duration = duration;
		this.artistList = new ArrayList<>();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public AlbumDTO getAlbum() {
		return album;
	}
	public void setAlbum(AlbumDTO album) {
		this.album = album;
	}
	
	public List<ArtistDTO> getArtistList() {
		return artistList;
	}
	public void setArtistList(List<ArtistDTO> artistList) {
		this.artistList = artistList;
	}
	
	@Override
	public String toString() {
		return "Song [id=" + id + ", title=" + title + ", file=" + file + ", duration=" + duration + ", album=" + album
				+ "]";
	}
}
